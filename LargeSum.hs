import System.IO
import Data.List
import Data.Char

readNumbers str = [read x| x<- lines$str]
main= do
  str <- readFile "150num.txt"
  print$take 10 . show$sum$readNumbers str
