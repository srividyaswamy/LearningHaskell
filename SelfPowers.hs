import System.IO

filterElements = (filter p [1..1000])
             where
               p x = x `mod` 10 /= 0
selfPowers = sum[x^x | x <- filterElements]
main= do
  putStrLn$ reverse.take 10.reverse.show$selfPowers
