import System.IO

issqrt n = ceiling $ sqrt $ fromIntegral n
isFactor n f = rem n f == 0
isComposite x = or $ map(isFactor x)[2..issqrt x]
primeFactor n = maximum $ [x |x<- [1..issqrt n],rem n x == 0, not(isComposite x)]
main :: IO()
main = do
    print $ primeFactor 600851475143
