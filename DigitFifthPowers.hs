import System.IO
import Data.Char

num n =  sum.map (^5).map(fromIntegral.digitToInt). show $ n
list =  sum [x |x<-[10..1000000],num x == x]
main= do
  putStrLn $ show $ list
