import System.IO
isFactor n f = rem n f == 0
isComposite p =or $ map(isFactor p)[2..ceiling $ sqrt $ fromIntegral p]
primes = [2]++[x |x<-[2..200000],not(isComposite x)]
nprime = [x | let x = primes !! 10000]
main= do
  print $ nprime
  --return expression
