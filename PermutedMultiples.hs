import System.IO()
import Data.List

multiples n = map(n*)[2..6]
isPermuted x n = x `elem` permutations n
isPermutedMultiple n = and $ [isPermuted q r |x <- multiples n,let q = show x,let r = show n]
permutedMultiples = [y |y <-[100000..1000000],isPermutedMultiple y]
main = do
    print $ minimum$permutedMultiples
