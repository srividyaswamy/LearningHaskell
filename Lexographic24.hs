import System.IO()
import Data.List

lexographicPermutations n = sort$permutations r
    where r = show n
main = do
  print$lexographicPermutations 1023456789 !! 999999
