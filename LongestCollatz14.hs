import System.IO
import Data.List
collatz 1 = 1
collatz n = if even n then n`div`2 else 3*n+1
collatzseq  = terminate . iterate collatz
              where
               terminate (1:_) = [1]
               terminate (x:xs) = x:terminate xs
collatzseqlength = map(length. collatzseq )[2..999999]
longestCollatzseq xs= head$filter((== maximum xs) . (xs !!)) [0..]
main= do
    print$(longestCollatzseq $collatzseqlength)+ 1
