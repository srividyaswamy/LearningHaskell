isFactor n f = rem n f == 0
isqrt n = ceiling $ sqrt $ fromIntegral n
isComposite p =or $ map(isFactor p)[2..isqrt p]
primes n = sum $ [2]++[x |x<-[2..isqrt n],not(isComposite x)]

main :: IO()
main= do
  print $ primes 2000000
