import System.IO()
import Data.List
import Data.Char

isPandigital n = toDigits n == [1..numlength n]
toDigits n= sort$map(fromIntegral.digitToInt).show $ n
numlength n = length$toDigits n
isFactor n f = rem n f == 0
isqrt n = ceiling $ sqrt $ fromIntegral n
isComposite p =or $ map(isFactor p)[2..isqrt p]
prime  x = not(isComposite x)
pandigitalPrime n = isPandigital n && prime n
largestPanPrime= maximum $ [x | x <-[1..10000000],pandigitalPrime x]
main = do
   print $ largestPanPrime
