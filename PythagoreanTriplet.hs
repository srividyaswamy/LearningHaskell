import System.IO()


triplets limit = product $ head[[a,b,c] | a<-[2..limit],b<-[2..limit],c<-[2..limit],c^2 == a^2+b^2,a+b+c ==limit]
main= do
  let tripletproduct = triplets 1000
  putStrLn $ show $ tripletproduct
