ispalindrome n = reverse n == n
largestproduct = maximum[p |x<-[100..999],y<-[100..999],let p= x*y,let z = show p,ispalindrome z]

main= do
  print $ largestproduct
