import System.IO()
import Data.Char (digitToInt)

powerresult n = 2 ^ n

toDigits :: Integer -> [Int]
toDigits n = map(fromIntegral.digitToInt). show $ powerresult n

resultsum ::Integer -> Int
resultsum n = sum $ toDigits n
main= do
  putStrLn $ show $ resultsum 1000
