import System.IO()
import Data.Char

factorial 0 = 1;
factorial n = n * factorial(n-1)
toDigits n = map(fromIntegral.digitToInt).show $ factorial n
digitsSum n = sum $ toDigits n
main= do
  putStrLn $ show $ digitsSum 100
