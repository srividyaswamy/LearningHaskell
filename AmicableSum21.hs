import System.IO

properDivisors n = sum[x |x <-[1..n-1],rem n x == 0]
isamicableNumber n = (properDivisors$properDivisors n) == n && n /= properDivisors n
amicableNumbers = [x |x <-[1..10000], isamicableNumber x]
main = do
     print$sum$amicableNumbers
