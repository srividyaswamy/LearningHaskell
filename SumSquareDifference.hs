square_sum= sum $ map(^2)[1..100]
sum_square = (^2) $ sum $ [1..100]

main= do
  let sumSquareDiff = sum_square - square_sum
  putStrLn $ show sumSquareDiff
