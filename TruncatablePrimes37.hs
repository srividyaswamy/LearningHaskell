import System.IO
import Data.Char

isFactor n f = rem n f == 0
isComposite 2 = False
isComposite p =if p/= 1 then or $ map(isFactor p)[2..ceiling $ sqrt $ fromIntegral p] else
               True
toNum = foldl addDigit 0
  where addDigit num d = 10*num + d

toDigit x= map(fromIntegral.digitToInt).show $ x

numLength x = length$toDigit x

leftTruncate x = [toNum$drop i $ toDigit x | i <- [0..numLength x-1]]

rightTruncate x = [toNum$take i $ toDigit x| i<- [1..numLength x]]

isPrime n = and$[not(isComposite x)&& not(isComposite y) | x<-leftTruncate n,
                  y<-rightTruncate n]
truncatablePrimes = take 11$[x | x<-[11..],isPrime x]
main= do
  print$truncatablePrimes
  print$sum$truncatablePrimes
