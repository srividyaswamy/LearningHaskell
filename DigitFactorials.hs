import System.IO()
import Data.Char

factorial 0 = 1
factorial n = n * factorial (n-1)
toDigitsFact n = map(factorial).map(fromIntegral.digitToInt).show $ n
digitFactSum x = sum $ toDigitsFact x
digitFactorial = sum$[x | x <- [3..100000],digitFactSum x == x]
main= do
    print$ digitFactorial
