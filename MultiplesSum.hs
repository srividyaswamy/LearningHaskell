import System.IO

multipleSum m n = sum $[x|x<-[1..999], x `mod`m == 0 || x `mod` n == 0]
main :: IO()
main = do
        val1 <- readLn
        val2 <- readLn
        let sum =  multipleSum val1 val2
        print sum
