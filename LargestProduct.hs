import Data.Char
import Data.List
import System.IO

products str = maximum . map product. foldr (zipWith (:)) (repeat []). take 13 . tails . map (fromIntegral . digitToInt). concat . lines $ str

main :: IO()
main= do
  str <- readFile "number.txt"
  let maxproduct = products str
  putStrLn $ show maxproduct
