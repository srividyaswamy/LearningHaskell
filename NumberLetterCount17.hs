import System.IO
import Data.Char

list1 = ["one","two","three","four","five","six","seven","eight","nine","ten",
         "eleven","twelve","thirteen","fourteen","fifteen","sixteen","seventeen",
         "eighteen","nineteen"]
list2 = ["twenty","thirty","forty","fifty","sixty","seventy","eighty","ninety"]

toDigit x = map(fromIntegral.digitToInt).show $ x
smaller x = rem x 100
name x
    | x < 20 = list1 !! (x-1)
    | x >= 20 && x < 100 && x `mod` 10 /= 0
      = list2 !! ((head$toDigit x)-2) ++ list1 !!((head$tail$toDigit x)-1)
    | x `mod` 10 == 0 && x < 100
      = list2 !! ((head$toDigit x)-2)
    | x `mod` 100 == 0 && x /= 1000
     = list1 !! ((head$toDigit x)-1)  ++ "hundred"
    |x `mod` 100 == 10
    = list1!! ((head$toDigit x)-1) ++ "hundredandten"
    | x `mod` 10 == 0 && x > 100 && x /= 1000
     = list1!!((head$toDigit x)-1) ++ "hundredand" ++ list2 !!((head$tail$toDigit x)-2)
    | x > 100 && x `mod` 10 /= 0
    = if(smaller x < 20) then list1!!((head$toDigit x)-1) ++ "hundredand"
      ++ list1!!(smaller x -1)
      else list1!!((head$toDigit x)-1) ++ "hundredand" ++ list2 !!((head$tail$toDigit x)-2)
      ++ list1!!((head$tail$tail$toDigit x)-1)
    | x == 1000
    = "onethousand"
totalCount = [length$name x| x<-[1..1000]]
main= do
  print $sum$totalCount
