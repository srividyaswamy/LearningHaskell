import System.IO()
import Numeric
import Data.Char

ispalindrome n = reverse n == n
tobinarydigit x = showIntAtBase 2 intToDigit x ""
doubleBasePalindrome = [x |x<-[1..999999], let y = show x,let z = tobinarydigit x,ispalindrome y && ispalindrome z]
main= do
  putStrLn $ show $ sum doubleBasePalindrome
