import System.IO

fib = 1:2:zipWith(+)fib(tail fib)
sumfib = sum $ takeWhile(<4000000) [x|x<-fib,x `rem` 2 == 0]

main = do
  print $ sumfib
