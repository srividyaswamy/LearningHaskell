import System.IO
import Data.List
import Data.Char

toNum = foldl addDigit 0
   where addDigit num d = 10*num + d
shift x n = drop n x ++ take n x
rotations x = [shift x i | i <-[1..(length x)]]
toDigits x = map(fromIntegral.digitToInt).show $ x
isCircularPrime x = and$[not(isComposite q)|y<-rotations$toDigits x,let q = toNum y]
isFactor n f = rem n f == 0
isComposite p = or $ map(isFactor p)[2..ceiling $ sqrt $ fromIntegral p]
circularPrime = 2:[x | x<-[2..1000000],not(isComposite x) && isCircularPrime x]

main= do
    print$circularPrime
    print$length$circularPrime
