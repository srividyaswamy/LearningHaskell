import System.IO
import Data.Char
import Data.List

toDigit x = map(fromIntegral.digitToInt).show $ x
fib = 1:1:zipWith(+)fib(tail fib)
fibonacci = head$[elemIndex x fib |x<-fib,(length$toDigit x) == 1000]
main = do
  print$fibonacci
